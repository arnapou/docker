FROM httpd:2.4
ENV IMAGE_NAME=httpd:2.4

RUN usermod -u 1000 www-data
RUN groupmod -g 1000 www-data

# --------- SSL / Certbot

RUN apt-get update && apt-get install -y \
    certbot \
    && rm -rf /var/lib/apt/lists/*

# --------- Apache config

COPY conf/default.modules.conf conf/default.other.conf /usr/local/apache2/conf/

RUN (echo "Include /usr/local/apache2/conf/default.modules.conf" \
  && echo "Include /usr/local/apache2/conf/default.other.conf" \
  && echo "Include /usr/local/apache2/conf/sites/*.conf" \
    ) >> /usr/local/apache2/conf/httpd.conf

# --------- Generic

RUN usermod -u 1000 www-data
RUN groupmod -g 1000 www-data

RUN mkdir -p /var/www/html \
 && mkdir -p /var/log/apache2 \
 && mkdir -p /usr/local/apache2/conf/sites \
 && chown www-data:www-data /var/www/html \
 && chmod 777 -R /var/log/apache2

# --------- bashrc

COPY .bashrc /tmp/.bashrc
RUN cat /tmp/.bashrc >> /etc/bash.bashrc
RUN rm -f /tmp/.bashrc
