.PHONY: help
help:
	@egrep -e '^[[:alnum:]][[:alnum:]\.-]+\:' $(MAKEFILE_LIST) | grep -v '^help' | cut -d':' -f1 | sort

ifeq (,$(wildcard .env))
$(error File '.env' does not exists, look at '.env.dist' example)
endif

ORGA_NAME=registry.gitlab.com/arnapou/docker

# ————————————————————————————————————————

%: export TAG = $(FILE)$(if $(strip $(TARGET)),-$(TARGET),)
%: export OPT = $(if $(strip $(TARGET)),--target=$(TARGET),)
%:
	@echo "\033[47;90m 📦  Building & pushing \033[47;1;94m$(ORGA_NAME)/\033[47;1;91m$(REPO):$(TAG)  \033[0m"
	docker build -t $(ORGA_NAME)/$(REPO):$(TAG) -f $(REPO)/$(FILE).Dockerfile $(REPO) $(OPT) && \
	docker --config . push $(ORGA_NAME)/$(REPO):$(TAG)

# ————————————————————————————————————————

login:
	@docker --config . login $(ORGA_NAME)

php-%: export REPO = php
php-%: export FILE = $(word 2,$(subst -, ,$@))
php-%: export TARGET = $(word 3,$(subst -, ,$@))

php-5.6: php-5.6-apache ;
php-7.0: php-7.0-apache ;
php-7.1: php-7.1-apache ;
php-7.2: php-7.2-apache ;
php-7.3: php-7.3-fpm php-7.3-cron php-7.3-dev ;
php-7.4: php-7.4-fpm php-7.4-cron php-7.4-dev ;
php-8.0: php-8.0-fpm php-8.0-cron php-8.0-dev ;
php-8.1: php-8.1-fpm php-8.1-cron php-8.1-dev ;
php-8.2: php-8.2-frankenphp php-8.2-cron php-8.2-dev ;
php-8.3: php-8.3-frankenphp php-8.3-cron php-8.3-dev ;
php-8.4: php-8.4-frankenphp php-8.4-cron php-8.4-dev ;

# ————————————————————————————————————————

httpd-%: export REPO = httpd

httpd-2.4: export FILE = 2.4
