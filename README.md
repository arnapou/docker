Arnapou docker images
===========================

I store here the docker images I built and used into personal project, for dev or for my hosted projects.

🐋 Images
---------------------------

```
registry.gitlab.com/arnapou/docker/php:8.3-dev
registry.gitlab.com/arnapou/docker/php:8.3-cron
registry.gitlab.com/arnapou/docker/php:8.3-fpm
registry.gitlab.com/arnapou/docker/php:8.3-frankenphp

registry.gitlab.com/arnapou/docker/php:8.2-dev
registry.gitlab.com/arnapou/docker/php:8.2-cron
registry.gitlab.com/arnapou/docker/php:8.2-fpm
registry.gitlab.com/arnapou/docker/php:8.2-frankenphp

registry.gitlab.com/arnapou/docker/php:8.1-dev
registry.gitlab.com/arnapou/docker/php:8.1-cron
registry.gitlab.com/arnapou/docker/php:8.1-fpm

registry.gitlab.com/arnapou/docker/php:8.0-dev
registry.gitlab.com/arnapou/docker/php:8.0-cron
registry.gitlab.com/arnapou/docker/php:8.0-fpm

registry.gitlab.com/arnapou/docker/php:7.4-dev
registry.gitlab.com/arnapou/docker/php:7.4-cron
registry.gitlab.com/arnapou/docker/php:7.4-fpm

registry.gitlab.com/arnapou/docker/php:7.3-dev
registry.gitlab.com/arnapou/docker/php:7.3-cron
registry.gitlab.com/arnapou/docker/php:7.3-fpm

registry.gitlab.com/arnapou/docker/php:7.2-apache
registry.gitlab.com/arnapou/docker/php:7.1-apache
registry.gitlab.com/arnapou/docker/php:7.0-apache
registry.gitlab.com/arnapou/docker/php:5.6-apache

registry.gitlab.com/arnapou/docker/httpd:2.4
```

⚙️ Notes about docker install
---------------------------

### [docker](https://docs.docker.com/engine/install/ubuntu/#install-using-the-convenience-script)

```bash
curl -fsSL https://get.docker.com | sh
```

### [docker-compose](https://docs.docker.com/compose/install/) ([releases](https://github.com/docker/compose/releases))

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/v2.14.0/docker-compose-linux-x86_64" -o /usr/local/bin/docker-compose
sudo chmod a+x /usr/local/bin/docker-compose 
```

### login

```bash
docker login registry.gitlab.com
```

### [config non root](https://docs.docker.com/install/linux/linux-postinstall/)

```bash
sudo groupadd docker
sudo usermod -aG docker ${USER}
newgrp docker
sudo systemctl enable docker
```

### portenair

http://127.0.0.1:8900/
```bash
docker volume create portainer_data
docker run -d -p 8000:8000 -p 8900:9000 --name=portainer --restart=always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v portainer_data:/data portainer/portainer
```

### [buildx](https://github.com/docker/buildx/)

```bash
mkdir -p ~/.docker/cli-plugins/
wget -O ~/.docker/cli-plugins/docker-buildx https://github.com/docker/buildx/releases/download/v0.8.2/buildx-v0.8.2.linux-amd64
chmod a+x ~/.docker/cli-plugins/docker-buildx
```


📁 Mount paths
---------------------------

| context      | path                            | info                                  |
|--------------|---------------------------------|---------------------------------------|
| caddy        | `/app`                          | application base dir                  |
| caddy        | `/app/public`                   | http document root                    |
| php          | `/cache`                        | cache directory (composer cache @dev) |
| php          | `/tmp/php`                      | php sys_get_temp_dir                  |
| php          | `/var/log/php`                  | php error_log                         |
| cron         | `/var/log/supervisor`           | supervisor logs                       |
| httpd/apache | `/var/www/html`                 | http document root                    |
| httpd/apache | `/var/log/apache2`              | apache logs                           |
| httpd/apache | `/usr/local/apache2/conf/sites` | apache custom conf                    |
