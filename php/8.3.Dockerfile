FROM dunglas/frankenphp:1.4.2-php8.3-alpine AS frankenphp
ENV IMAGE_NAME "php${PHP_VERSION}-frankenphp"
ENV CADDY_GLOBAL_OPTIONS "auto_https off"
ENV SERVER_NAME ":80"

# https://github.com/mlocati/docker-php-extension-installer/releases/
ADD --chmod=0755 https://github.com/mlocati/docker-php-extension-installer/releases/download/2.7.14/install-php-extensions /usr/local/bin/

# --------- Base

RUN install-php-extensions \
    bcmath    dom       gd        imagick   intl      mbstring  redis     iconv     \
    pcntl     session   soap      sockets   yaml      zip       opcache   memcached \
    mongodb   pdo       mysqli    pdo_mysql pgsql     pdo_pgsql

COPY conf/php.ini $PHP_INI_DIR/conf.d/00-arnapou.ini
COPY conf/php-jit.ini $PHP_INI_DIR/conf.d/00-arnapou-jit.ini
RUN cp $PHP_INI_DIR/php.ini-production $PHP_INI_DIR/php.ini \
 && ln -s /usr/local/bin/php /usr/bin/php

RUN apk update && apk add bash shadow && apk cache clean \
 && usermod --uid 1000 www-data && groupmod --gid 1000 www-data \
 && mkdir -p -m 777 /cache /tmp/php /var/log/php

COPY conf/timezone /etc/timezone

ENV ENV /etc/profile
ENV SHELL /bin/bash
COPY conf/.bashrc /etc/profile.d/arnapou.sh
RUN echo ". /etc/profile.d/arnapou.sh" >> /etc/bash/bashrc

# --------- Caddy server

COPY conf/frankenphp83.Caddyfile /etc/caddy/Caddyfile
RUN rm -f /app/public/index.php

# ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

FROM frankenphp AS cron
ENV IMAGE_NAME "php${PHP_VERSION}-cron"

RUN apk add zip unzip curl supervisor \
 && mkdir -p /var/log/supervisor  \
 && chmod 777 /var/log/supervisor

# on wrappe supervisord pour exporter les variables d'env à usage des crontabs
ENV SHELL /bin/bash
COPY conf/supervisord.conf        /etc/supervisor/conf.d/crontab.conf
COPY conf/supervisord.wrapper.sh  /bin/supervisord.wrapper.sh
RUN chmod 755 /bin/supervisord.wrapper.sh

CMD ["/bin/supervisord.wrapper.sh"]

# ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

FROM frankenphp AS dev
ENV IMAGE_NAME "php${PHP_VERSION}-dev"

ENV COMPOSER_HOME /cache/composer
ENV COMPOSER_CACHE_DIR /cache/composer_cache

RUN apk add sudo make openssh wget git zip unzip curl vim rsync iputils netcat-openbsd \
 && install-php-extensions pcov xdebug @composer \
 && mkdir -p -m 777 $COMPOSER_HOME $COMPOSER_CACHE_DIR \
 && chown -R www-data:www-data $COMPOSER_HOME $COMPOSER_CACHE_DIR \
 && echo 'alias composer="php -d memory_limit=-1 /usr/local/bin/composer"' >> /etc/profile.d/arnapou.sh \
 && echo 'www-data ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

COPY conf/xprofile /bin/xprofile
RUN chmod 755 /bin/xprofile
