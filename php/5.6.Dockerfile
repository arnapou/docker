# syntax = docker/dockerfile:1.4.0
FROM php:5.6-apache-jessie AS apache
ENV IMAGE_NAME "php:5.6-apache"

# --------- APT Jessie est dead !

# Avant
#deb http://deb.debian.org/debian jessie main
#deb http://security.debian.org/debian-security jessie/updates main
#deb http://deb.debian.org/debian jessie-updates main

# Apres
#deb http://archive.debian.org/debian jessie main
#deb http://archive.debian.org/debian-security jessie/updates main
#deb http://archive.debian.org/debian main

RUN sed -i -e 's/deb.debian.org/archive.debian.org/g' \
           -e 's|security.debian.org|archive.debian.org/|g' \
           -e '/jessie-updates/d' /etc/apt/sources.list

# --------- APT

RUN apt-get update \
 && apt-get install -y --force-yes \
    sudo sshfs wget git unzip curl vim supervisor cron \
    zlib1g-dev libzip-dev libyaml-dev mcrypt libmcrypt-dev libxml2-dev \
    libjpeg62-turbo-dev libpng-dev libfreetype6-dev libpq-dev libmemcached-dev \
 && rm -rf /var/lib/apt/lists/*

# --------- PHP

# 2023.07.06 commenté car plante !
#RUN pecl install zip       && docker-php-ext-enable zip
RUN docker-php-ext-install gd

RUN docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd && docker-php-ext-install pdo_mysql
RUN docker-php-ext-install mysql

RUN mkdir -p /etc/php/5.6/apache2/conf.d/
RUN <<EOF cat > /etc/php/5.6/apache2/conf.d/php_docker.ini
log_errors=On
date.timezone="Europe/Paris"
EOF

# --------- Apache

RUN a2enmod rewrite
RUN a2enmod expires
RUN a2enmod ssl
RUN a2enmod headers

# --------- User

RUN usermod -u 1000 www-data
RUN echo "www-data ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
RUN chown www-data /var/www

# --------- bashrc

COPY conf/.bashrc /tmp/.bashrc
RUN cat /tmp/.bashrc >> /etc/bash.bashrc
RUN rm -f /tmp/.bashrc

# --------- Composer

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --1 &&\
    chmod +x /usr/local/bin/composer

# --------- Config

RUN <<EOF cat > /etc/apache2/sites-available/000-default.conf
<VirtualHost *:80>
    DocumentRoot /var/www/html
    <Directory /var/www/html>
        Options -Indexes +FollowSymLinks +Includes
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF

RUN <<EOF cat > /etc/supervisor/conf.d/apache.conf
[supervisord]
nodaemon=true

[program:apache]
command=apache2ctl -D "FOREGROUND"
autostart=true
autorestart=true
startretries=10
startsecs=5
redirect_stderr=true
killasgroup=true
stopasgroup=true
EOF

# --------- Final

USER root

CMD ["/usr/bin/supervisord"]
