if [ "`id -u`" -eq 0 ]; then
    PS1='\[\033[01;31m\]\u@\h${IMAGE_NAME:+.$IMAGE_NAME}\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='\[\033[01;32m\]\u@\h${IMAGE_NAME:+.$IMAGE_NAME}\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
    PS1="\[\e]0;\u@\h: \w\a\]$PS1"
fi

alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
