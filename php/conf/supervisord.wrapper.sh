#!/bin/bash
set -e

# 👉️ Cette image calc contient supervisord + cron.
# — les variables d'environnement ne sont pas propagées aux crons !
# — donc dans le cas de conteneurs docker, c'est quand même assez ballot
#
# 👉️ La solution appliquée tient avec plusieurs conditions
# — Dockerfile -> ENV SHELL /bin/bash
#              mettre le shell par défaut à bash
#
# — Dockerfile -> CMD ["/bin/supervisord.wrapper.sh"]
#              wrapper l'entrypoint pour exporter les variables
#
# — Ici        -> /bin/sh -c 'export > /env.sh'
#              exporter toutes les variables dans un fichier
#
# — Crontab    -> * * * * * www-data . /env.sh; /ma/commande >> /ma/log.log 2>&1
#              exportation des variables juste avant l'exécution
#              !! Attention : le point devant /env.sh est important, c'est
#              un alias du builtin bash 'source'.
#              cf. https://www.computerhope.com/unix/bash/source.htm
#
# ────────────────────────────────────────────────────────────────────────────
# On utilise volontairement sh qui produit une vraie succession d'export
# comme on le désire.
# !! Ne fonctionne pas avec bash et zsh par exemple
#
/bin/sh -c 'export > /env.sh'

# ────────────────────────────────────────────────────────────────────────────
# Quelques droits propre "au cas où"
#
chown www-data:www-data /env.sh
chmod 600 /env.sh

# ────────────────────────────────────────────────────────────────────────────
# Check des droits des fichiers crontab au start (debian + alpine)
#
function reset_perms {
    if [[ -d $1 ]]; then
        for file in $(find $1 -type f) ; do
            chown root:root ${file}
            chmod 600       ${file}
        done
    fi
    true
}
for p in d daily hourly monthly weekly; do reset_perms /etc/cron.$p ; done
for p in 15min daily hourly monthly weekly; do reset_perms /etc/periodic/$p ; done

# ────────────────────────────────────────────────────────────────────────────
# Fallback for cron

if [[ ! -e /usr/sbin/crond && -x /usr/sbin/cron ]]; then
  ln -s /usr/sbin/cron /usr/sbin/crond
fi

# ────────────────────────────────────────────────────────────────────────────
# "exec" is a builtin command of the Bash shell.
# It allows you to execute a command that completely replaces the current
# process. The current shell process is destroyed, and entirely replaced by
# the command you specify.
# cf. https://www.computerhope.com/unix/bash/exec.htm
#
if [[ -f /etc/supervisor/supervisord.conf ]]; then
  SUPERVISORD_CONF=/etc/supervisor/supervisord.conf
elif [[ -f /etc/supervisord.conf ]]; then
  SUPERVISORD_CONF=/etc/supervisord.conf
  if [[ -d /etc/supervisor/conf.d ]]; then
    echo "files = /etc/supervisor/conf.d/*.conf" >> /etc/supervisord.conf
  fi
else
  echo "ERROR: unable to find the supervisord configuration file."
  exit 1
fi

exec /usr/bin/supervisord \
  --nodaemon \
  --logfile /var/log/supervisor/supervisor.log \
  --configuration $SUPERVISORD_CONF
