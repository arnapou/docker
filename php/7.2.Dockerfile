FROM php:7.2-apache AS apache
ENV IMAGE_NAME "php${PHP_VERSION}-apache"

RUN curl -sSLf -o /usr/local/bin/install-php-extensions \
    https://github.com/mlocati/docker-php-extension-installer/releases/download/2.6.4/install-php-extensions \
 && chmod +x /usr/local/bin/install-php-extensions

# --------- Base

RUN install-php-extensions \
    bcmath    dom       gd        imagick   intl      mbstring  redis     iconv     \
    pcntl     session   soap      sockets   yaml      zip       opcache   memcached \
    mongodb   pdo       mysqli    pdo_mysql pgsql     pdo_pgsql mcrypt

COPY conf/php.ini $PHP_INI_DIR/conf.d/00-arnapou.ini
RUN cp $PHP_INI_DIR/php.ini-production $PHP_INI_DIR/php.ini \
 && ln -s /usr/local/bin/php /usr/bin/php

RUN usermod --uid 1000 www-data && groupmod --gid 1000 www-data \
 && mkdir -p -m 777 /cache /tmp/php /var/log/php

COPY conf/timezone /etc/timezone

ENV ENV /etc/profile
ENV SHELL /bin/bash
COPY conf/.bashrc /etc/profile.d/arnapou.sh
RUN echo ". /etc/profile.d/arnapou.sh" >> /etc/bash.bashrc

# --------- Composer

ENV COMPOSER_HOME /cache/composer
ENV COMPOSER_CACHE_DIR /cache/composer_cache

RUN apt-get update \
 && apt-get install -y --no-install-recommends \
    sudo sshfs wget git zip unzip curl vim rsync netcat iputils-ping \
 && rm -rf /var/lib/apt/lists/* \
 && install-php-extensions pcov @composer \
 && mkdir -p -m 777 $COMPOSER_HOME $COMPOSER_CACHE_DIR \
 && chown -R www-data:www-data $COMPOSER_HOME $COMPOSER_CACHE_DIR \
 && echo 'alias composer="php -d memory_limit=-1 /usr/local/bin/composer"' >> /etc/bash.bashrc \
 && echo 'www-data ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

# --------- Apache

RUN <<EOF cat > /etc/apache2/sites-available/000-default.conf
<VirtualHost *:80>
    DocumentRoot /var/www/html
    <Directory /var/www/html>
        Options -Indexes +FollowSymLinks +Includes
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF

RUN a2enmod rewrite
RUN a2enmod expires
RUN a2enmod ssl
RUN a2enmod headers

# --------- Final

USER root

# --------- Supervisor

RUN apt-get update \
 && apt-get install -y --no-install-recommends \
    supervisor cron \
 && rm -rf /var/lib/apt/lists/* \
 && mkdir -p /var/log/supervisor  \
 && chmod 777 /var/log/supervisor

# on wrappe supervisord pour exporter les variables d'env à usage des crontabs
ENV SHELL /bin/bash
COPY conf/supervisord.conf        /etc/supervisor/conf.d/crontab.conf
COPY conf/supervisord.wrapper.sh  /bin/supervisord.wrapper.sh
RUN chmod 755 /bin/supervisord.wrapper.sh

CMD ["/bin/supervisord.wrapper.sh"]

RUN <<EOF cat > /etc/supervisor/conf.d/apache.conf
[supervisord]
nodaemon=true

[program:apache]
command=apache2ctl -D "FOREGROUND"
autostart=true
autorestart=true
startretries=10
startsecs=5
redirect_stderr=true
killasgroup=true
stopasgroup=true
EOF
